/*

By Paulão da Regulagem (https://t.me/Paulera6)

Copia não comédia!

*/
'use strict';
const request = require('request');
const poll = require('promise-poller').default;

const fetch = require('node-fetch');
const fetchCookie = require('fetch-cookie')(fetch)
const CAP_KEY = 'dd8002769b059d976f91aefe9693cf43';

module.exports = async (cpf, pass, KEY) =>{
    console.log('FGTS =>', cpf, pass);

    try{

        if( !KEY )
            KEY = CAP_KEY;

        const registerCaptcha = await registerCap();
        const captchaSolved = await chkCap(KEY, registerCaptcha.request);



        var cookie = await fetch('https://fgts.caixa.gov.br/n71TRL5BiFkdaldRmr1Ix3uTqEFFfiqIghdzsZ1Qect4DNMq5Dzr7kBooHtK/pages/inter/home.jsp', {
            headers: {
                'Connection': 'keep-alive',
                'Cache-Control': 'max-age=0',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'Sec-Fetch-Site': 'none',
                'Sec-Fetch-Mode': 'navigate',
                'Sec-Fetch-User': '?1',
                'Sec-Fetch-Dest': 'document',
                'Accept-Language': 'pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7,es-US;q=0.6,es;q=0.5',
                'Cookie': '_fbp=fb.2.1604511364855.1325417885; _ga=GA1.3.1870424190.1604511365; _gid=GA1.3.1910093843.1604511365'
            }
        });


        console.log('COOKIES', await cookie.headers.get('set-cookie'))


        let getNIS = await fetch('https://fgts.caixa.gov.br/n71TRL5BiFkdaldRmr1Ix3uTqEFFfiqIghdzsZ1Qect4DNMq5Dzr7kBooHtK/S1', {
            body: 'req={"tipo":"cpf","nasc":null,"valor":"'+cpf+'","g-recaptcha-response":"'+captchaSolved+'","versao":"1.1.46","dispositivo":"Browser","compilacao":1}',
            method: 'POST',
            headers: {
                'Connection': 'keep-alive',
                'Accept': 'application/json, text/javascript, */*; q=0.01',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'Origin': 'https://fgts.caixa.gov.br',
                'Sec-Fetch-Site': 'same-origin',
                'Sec-Fetch-Mode': 'cors',
                'Sec-Fetch-Dest': 'empty',
                'Referer': 'https://fgts.caixa.gov.br/n71TRL5BiFkdaldRmr1Ix3uTqEFFfiqIghdzsZ1Qect4DNMq5Dzr7kBooHtK/pages/inter/home.jsp',
                'Accept-Language': 'pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7,es-US;q=0.6,es;q=0.5',
                'Cookie' : await cookie.headers.get('set-cookie'),
            },

        });

        let info = await getNIS.json();


        console.log('INFO =>', info);
       

       if( info.msg ){
            if( info.msg.indexOf('não encontrado') > 0 || !info.nis){
                return info;
            }
        }

        let getBalance = await fetch('https://fgts.caixa.gov.br/n71TRL5BiFkdaldRmr1Ix3uTqEFFfiqIghdzsZ1Qect4DNMq5Dzr7kBooHtK/S2', {
            body: 'req=%7B%22nis%22%3A%22'+encodeURIComponent(info.nis)+'%22%2C%22senha%22%3A%22'+pass+'%22%2C%22versao%22%3A%221.1.46%22%2C%22dispositivo%22%3A%22Browser%22%2C%22compilacao%22%3A1%7D',
            method: 'POST',
            headers: {
                'Connection': 'keep-alive',
                'Accept': 'application/json, text/javascript, */*; q=0.01',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'Origin': 'https://fgts.caixa.gov.br',
                'Sec-Fetch-Site': 'same-origin',
                'Sec-Fetch-Mode': 'cors',
                'Sec-Fetch-Dest': 'empty',
                'Referer': 'https://fgts.caixa.gov.br/n71TRL5BiFkdaldRmr1Ix3uTqEFFfiqIghdzsZ1Qect4DNMq5Dzr7kBooHtK/pages/inter/home.jsp',
                'Accept-Language': 'pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7,es-US;q=0.6,es;q=0.5',
                'Cookie' : await cookie.headers.get('set-cookie'),
                // 'Cookie' : '_fbp=fb.2.1604511364855.1325417885; _ga=GA1.3.1870424190.1604511365; _gid=GA1.3.1910093843.1604511365; _gat_UA-71411161-12=1'
            },

        });
       let resp = await getBalance.json();

        return resp;




        async function chkCap(
            key,
            id,
            retries = 150,
            interval = 1500,
            delay = 5000
        ) {
            const timeout = millis => new Promise(resolve => setTimeout(resolve, millis))

            await timeout(delay);
            return poll({
                taskFn: requestCaptchaResults(key, id),
                interval,
                retries
            });
        }

        function requestCaptchaResults(apiKey, requestId) {
            const url = `http://2captcha.com/res.php?key=${apiKey}&action=get&id=${requestId}&json=1`;
            return async function() {
                return new Promise(async function(resolve, reject) {
                    const rawResponse = await fetch(url);
                    const resp = await rawResponse.json();

                    console.log('[FGTS] CAP RES =>', resp)
                    if (resp.status === 0) return reject(resp.request);
                    resolve(resp.request);
                });
            }
        }
        async function registerCap() {
            const response = await fetch(`http://2captcha.com/in.php?key=` + KEY + `&method=userrecaptcha&googlekey=6LdvBBcUAAAAAB0zBD2-iV1OXHU514RgicFQ5nLy&pageurl=https://acessoseguro.caixa.gov.br/internet.do?segmento=CONVENIADO01&template=simulador&urlCallback=https://www.conectividadesocial.caixa.gov.br/sifug-web/principal.fug&action=verify&json=1`);
            return await response.json();
        }
    }catch(err){
        console.log(err);
    }
}

