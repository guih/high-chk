var express = require('express');
var chk = require('../lib/CaixaTem');
var FGTS = require('../lib/FGTS');

var router = express.Router();

/* GET home page. */
router.get('/', async function(req, res, next) {
  try {
  	// var f = await FGTS('a', 'b');
  	// console.log(f, "<<<")
 	 res.render( 'index', { title: 'Express', id: Math.random().toString(36).substr(2, 9) });

	// console.log(res);
  } catch (err) {
    console.error(err);
  }
});

module.exports = router;
